# EVERPRESS - Skills Assessment

Role: Full Stack Developer

**Note: Information communicated is strictly confidential and can't be shared outside the scope of the exercise.**

The full stack developer interview will focus on things that the team work on every day across the stack:

- Exposing the APIs that the frontend will consume and displaying this information for users
- Integrating with many external services and printing companies for production
- Pricing, printing and other important business logic
- Building new features and improving the overall experience for users

As part of the interview please complete this skills assessment.

The assessment comprises of two exercises: 

1. Backend Exercise - Creating an order API
2. Frontend Exercise - Consuming order APIs to display information on a UI

## Instructions:

- Please fork the repository, adding your initials to the name (fullstack-exercise-ab)
- Please spend up to 2h on the exercise, focusing on the tasks you're most comfortable with
- It's ok if you don't complete all the tasks
- When you are done, please push your work and share your repo URL with us at least 24h before your interview, so we can review it in advance
- Reach out to Nnenna if you have any questions, she can pass these on to the team
